package com.paychex.wlaw.timesheetz

import com.sun.org.glassfish.gmbal.Description
import geb.Browser
import geb.Page
import groovy.time.Duration
import org.yaml.snakeyaml.Yaml

import java.sql.Time
import java.time.LocalDate

class FirstSignInPage extends Page {
    static url = "https://ppm.paychex.com/itg/dashboard/app/portal/PageView.jsp"

    static content = {
        loginButton(to: SecondSignInPage) { $("form", name: "Login").$("input", value: "Log In") }
        username { $("form", name: "Login").$("input", name: "USER") }
        password { $("form", name: "Login").$("input", name: "PASSWORD") }
    }
}

class SecondSignInPage extends Page {
    static at = { title == "PPM Logon" }

    static content = {
        loginButton(to: LandingPage) { $("div#label-LOGON_SUBMIT_BUTTON_CAPTION") }
        username { $("form", name: "logon").$("input", name: "USERNAME") }
        password { $("form", name: "logon").$("input", name: "PASSWORD") }
    }
}

class LandingPage extends Page {
    static at = { waitFor(10) { title == "Dashboard - My Time and Work" }}
    static content = {
        timeSheetTable(waitCondition: { it.displayed }) { $("div#div_inner_frame_1310750") }
        newTimeSheetButton(waitCondition: { it.displayed }, to: NewTimeSheetPage) { $("span.btnCreateNewTS a")}
    }
}

class NewTimeSheetPage extends Page {
    static at = { title == "Create Time Sheet" }
    static content = {
        createButton(to: EditTimeSheetPage) { $("div#createButton") }
        includeItemsCheckbox { $("input#includeMyItems") }
    }
}

class EditTimeSheetPage extends Page {
    static at = { title == "Edit Time Sheet" }

    static content = {
        addItemsButton { $("div#addWorkItems") }
        addItemsChoices { $("div", id: "addWorkItems.choices") }
        addRequestButton { $("div", id: "addWorkItems.REQUEST") }

        addRequestFrame { $("#anotherItemDialogIF") }
    }
}

class AddRequestFrame extends Page {
    static content = {
        requestNumberField { $("input", id: 'FLTR.P.REQ_NUM') }
        searchButton { $("a", id: "searchButton") }

        searchRows { $("tr", id: ~/search_table_row_\d+/) }
    }
}


Map test = new Yaml().load(new InputStreamReader(getClass().getResourceAsStream("/timesheet.yaml"))).template.requests

def password = System.getenv("PPM_PASSWORD")
def user = System.getenv("PPM_USER")
//return 0

Browser.drive {
    to FirstSignInPage
    username.value(user)
    password.value(password)
    loginButton.click()

    at SecondSignInPage
    username.value(user)
    password.value(password)
    loginButton.click()

    at LandingPage
    def timeSheets = timeSheetTable
            .find("tr")
            *.find("td")
            .collect { it*.text() }
            .drop(1)
            .dropRight(2)
            .collect {
                String[] numAndPeriod = it[0].split(" - ")
                def start = Date.parse("d/M/yy", numAndPeriod[1])
                def end = Date.parse("d/M/yy", numAndPeriod[2])
                return new TimeSheet(numAndPeriod[0].toInteger(), start, end, it[1], it[2])
            }
    println timeSheets

    newTimeSheetButton.click()

    at NewTimeSheetPage
    includeItemsCheckbox.value(false)
    createButton.click()

    at EditTimeSheetPage
    Map timeSheetTemplate = new Yaml().load(new InputStreamReader(getClass().getResourceAsStream("/timesheet.yaml"))).template
    timeSheetTemplate.requests?.each { Map.Entry entry ->
        entry.key
    }
    println timeSheetTemplate
    addItemsButton.click()
    waitFor { addItemsChoices.displayed }

    addRequestButton.click()

    withFrame(addRequestFrame, AddRequestFrame) {
        waitFor { requestNumberField.displayed }
        requestNumberField.value("50166")
        searchButton.click()
        def rows = searchRows.size()

        def h = 5
    }

}

class TimeSheet {
    TimeSheet(int number, Date start, Date end, String description, String status) {
        this.number = number
        this.start = start
        this.end = end
        this.description = description
        this.status = status
    }

    int number
    Date start
    Date end
    String description
    String status

    String toString() {
        return "$number|${start.format("dd/MM/yy")}-${end.format("dd/MM/yy")}|$description|$status"
    }

    enum Status {

    }
}