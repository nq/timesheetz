# timesheetz

Timesheet automator for PPM. 

## Usage

The best way to run this is to just open it in IntelliJ and run the groovy file. 

The `PPM_USER` and `PPM_PASSWORD` environment variables need to be set. 

The timesheet information is read from `src/main/resources/timesheet/yaml`. 